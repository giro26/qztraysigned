!include MUI2.nsh
!include x64.nsh
!include LogicLib.nsh

!ifdef NSIS_UNICODE
	!addplugindir "C:\tray/ant/windows/nsis/Plugins/Release_Unicode"
!else
	!addplugindir "C:\tray/ant/windows/nsis/Plugins/Release_ANSI"
!endif

!addincludedir "C:\tray/ant/windows/nsis/Include/"
!include StdUtils.nsh

Name "QZ Tray"
OutFile "C:\tray/out\qz-tray-community-2.1.0-RC7.exe"
RequestExecutionLevel admin

;-------------------------------

!define MUI_ICON "C:\tray\assets/branding\windows-icon.ico"

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_LANGUAGE "English"

;------------------------------

Section
  ; Sets the context of shell folders to "All Users"
  SetShellVarContext all

  ; Kills any running QZ Tray processes
  nsExec::ExecToLog "wmic.exe process where $\"Name like '%java%' and CommandLine like '%qz-tray.jar%'$\" call terminate"

  ; Set environmental variable for silent install to be picked up by cscript
  ${If} ${Silent}
    System::Call 'Kernel32::SetEnvironmentVariable(t, t)i ("qz_silent", "1").r0'
  ${EndIf}

  ; Cleanup for wmic on Windows XP
  SetShellVarContext current
  Delete "$DESKTOP\TempWmicBatchFile.bat"
  SetShellVarContext all

  SetOutPath "$INSTDIR"

  ; Cleanup resources from previous versions
  DetailPrint "Cleaning up resources from previous versions..."
  RMDir /r "$INSTDIR\demo\js\3rdparty"
  Delete "$INSTDIR\demo\js\qz-websocket.js"

  ; Remove 2.1 startup entry
  Delete "$SMSTARTUP\QZ Tray.lnk"

  File /r "C:\tray/out/dist\*"

  WriteRegStr HKLM "Software\QZ Tray" \
                     "" "$INSTDIR"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\QZ Tray" \
                   "DisplayName" "QZ Tray 2.1.0-RC7"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\QZ Tray" \
                     "Publisher" "QZ Industries, LLC"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\QZ Tray" \
                   "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\QZ Tray" \
                   "DisplayIcon" "$INSTDIR\windows-icon.ico"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\QZ Tray" \
                   "HelpLink" "https://qz.io/support"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\QZ Tray" \
                   "URLUpdateInfo" "https://qz.io/download"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\QZ Tray" \
                   "URLInfoAbout" "https://qz.io/support"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\QZ Tray" \
                   "DisplayVersion" "2.1.0-RC7"
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\QZ Tray" \
                   "EstimatedSize" "101459"

  ; Allow localhost connections for Microsoft Edge
  DetailPrint "Whitelisting loopback connections for Microsoft Edge..."
  nsExec::ExecToLog "CheckNetIsolation.exe LoopbackExempt -a -n=$\"Microsoft.MicrosoftEdge_8wekyb3d8bbwe$\""

  ; Mimetype support, e.g. qz:launch
  WriteRegStr HKCR "qz" "" "URL:QZ Tray Protocol"
  WriteRegStr HKCR "qz" "URL Protocol" ""
  WriteRegStr HKCR "qz\DefaultIcon" "" "$\"$INSTDIR\windows-icon.ico$\",1"
  WriteRegStr HKCR "qz\shell\open\command" "" "$\"$INSTDIR\qz-tray.exe$\" $\"%1$\""

  WriteUninstaller "$INSTDIR\uninstall.exe"

  ; Prevent launching exe from SysWOW64
  ${If} ${RunningX64}
  ${DisableX64FSRedirection}
  ${EndIf}

  ; Handle edge-case where jscript support is unregistered
  nsExec::ExecToLog "regsvr32.exe /s $\"%systemroot%\system32\jscript.dll$\""

  ; Remove QZ Industries, LLC certificates
  nsExec::ExecToLog "cscript.exe //NoLogo //E:jscript $\"$INSTDIR\auth\windows-keygen.js$\" $\"$INSTDIR$\" uninstall"

  ; Perform cleanup operations from previous install
  nsExec::ExecToLog "cscript.exe //NoLogo //E:jscript $\"$INSTDIR\utils\windows-cleanup.js$\" $\"QZ Tray$\""

  keygen:
  ; Exports a self-signed certificate and properties file
  DetailPrint "Generating a unique certificate for HTTPS support..."
  nsExec::ExecToLog "cscript.exe //NoLogo //E:jscript $\"$INSTDIR\auth\windows-keygen.js$\" $\"$INSTDIR$\" install"
  Pop $0

  ; Secure websockets is required, handle errors
  ${If} "$0" != "0"
    ${If} "$0" == "2"
      MessageBox MB_YESNO "Java is required for installation.  Download now?" IDYES true IDNO false
        true:
          ExecShell "open" "https://adoptopenjdk.net/?variant=openjdk11"
          MessageBox MB_OK "Click OK after Java is installed to resume installation"
          Goto keygen
        false:
          SetErrorLevel $0
          Abort "Failed while checking for Java 1.8"
    ${Else}
        Abort "Installation failed.  Please check log for details."
    ${EndIf}
  ${EndIf}

  ${If} ${RunningX64}
  ${EnableX64FSRedirection}
  ${EndIf}

  CreateShortCut "$SMPROGRAMS\QZ Tray.lnk" "$INSTDIR\qz-tray.exe" "" "$INSTDIR\windows-icon.ico" 0
  CreateShortCut "$SMSTARTUP\QZ Tray.lnk" "$INSTDIR\qz-tray.exe" "-A" "$INSTDIR\windows-icon.ico" 0

  ; Shared directory
  ReadEnvStr $R0 "PROGRAMDATA"
  CreateDirectory "$R0\qz"

  ; Grant R+W to Authenticated Users (S-1-5-11)
  AccessControl::GrantOnFile "$R0\qz" "(S-1-5-11)" "GenericRead + GenericWrite"

  ; Delete matching firewall rules
  DetailPrint "Removing QZ Tray firewall rules..."
  nsExec::ExecToLog "netsh.exe advfirewall firewall delete rule name= $\"QZ Tray$\""

  ; Install new Firewall rules
  DetailPrint "Installing QZ Tray inbound firewall rule..."
  nsExec::ExecToLog "netsh.exe advfirewall firewall add rule name=$\"QZ Tray$\" dir=in action=allow profile=any localport=8181,8282,8383,8484,8182,8283,8384,8485 localip=any protocol=tcp"

  ; Launch a non-elevated instance of QZ Tray
  ${StdUtils.ExecShellAsUser} $0 "$SMPROGRAMS\QZ Tray.lnk" "open" ""
SectionEnd

;-------------------------------

Section "Uninstall"
  ; Sets the context of shell folders to "All Users"
  SetShellVarContext all

  ; Kills any running QZ Tray processes
  nsExec::ExecToLog "wmic.exe process where $\"Name like '%java%' and CommandLine like '%qz-tray.jar%'$\" call terminate"

  ; Set environmental variable for silent install to be picked up by cscript
  ${If} ${Silent}
    System::Call 'Kernel32::SetEnvironmentVariable(t, t)i ("qz_silent", "1").r0'
  ${EndIf}

  ; Cleanup for wmic on Windows XP
  Delete "$DESKTOP\TempWmicBatchFile.bat"

  ; Prevent launching exe from SysWOW64
  ${If} ${RunningX64}
  ${DisableX64FSRedirection}
  ${EndIf}

  ; Remove QZ Industries, LLC certificates
  nsExec::ExecToLog "cscript.exe //NoLogo //E:jscript $\"$INSTDIR\auth\windows-keygen.js$\" $\"$INSTDIR$\" uninstall"

  ${If} ${RunningX64}
  ${EnableX64FSRedirection}
  ${EndIf}

  ; Remove startup entries
  nsExec::ExecToLog "cscript.exe //NoLogo //E:jscript $\"$INSTDIR\utils\windows-cleanup.js$\" $\"QZ Tray$\""

  ; Delete matching firewall rules
  DetailPrint "Removing QZ Tray firewall rules..."
  nsExec::ExecToLog "netsh.exe advfirewall firewall delete rule name= $\"QZ Tray$\""

  Delete "$SMPROGRAMS\QZ Tray.lnk"
  Delete "$INSTDIR\uninstall.exe"
  RMDir /r "$INSTDIR"

  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\QZ Tray"
  DeleteRegKey HKLM "Software\QZ Tray"

  ; Remove URI handler
  DeleteRegKey HKCR "qz"

  Delete "$DESKTOP\QZ Tray.url"
  Delete "$DESKTOP\QZ Tray.lnk"
  Delete "$SMSTARTUP\QZ Tray.lnk"

  ; Sets the context of shell folders to current user
  SetShellVarContext current
  Delete "$DESKTOP\QZ Tray.url"
  Delete "$DESKTOP\QZ Tray.lnk"
  Delete "$SMPROGRAMS\QZ Tray.lnk"
SectionEnd

;-------------------------------

Function .onInit
  ${If} ${RunningX64}
      SetRegView 64
  ${EndIf}
  ${If} $InstDir == ""
    ${If} ${RunningX64}
      StrCpy $INSTDIR "$PROGRAMFILES64\QZ Tray"
    ${Else}
      StrCpy $INSTDIR "$PROGRAMFILES\QZ Tray"
    ${EndIf}
  ${EndIf}
FunctionEnd

Function un.onInit
    ${If} ${RunningX64}
        SetRegView 64
    ${EndIf}
FunctionEnd

